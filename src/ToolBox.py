import tkinter as tk 
from tkinter import ttk
from tkinter.filedialog import askopenfilename

import numpy as np

import classic_beta_div as bd

class ToolBox(tk.Tk):

    def __init__(self):
        super().__init__()

        self.geometry("250x175")
        self.title("ToolBox")
        self.resizable(0, 0)

        # matrices
        self.V = None
        self.W = None
        self.H = None

        # method and parameters
        self.current_method = tk.StringVar()
        self.current_beta = tk.IntVar()
        self.current_alpha = tk.StringVar()
        self.current_alpha.set("1")

        # grid configuration
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

        self.create_widgets()

    def create_widgets(self):
        # matrixes
        V_button=tk.Button(self, text="V", command=self.fill_V)
        V_button.grid(column=0, row=0, sticky=tk.E, padx=5, pady=5)
        W_button=tk.Button(self, text="W", command=self.fill_W)
        W_button.grid(column=1, row=0, sticky=tk.W, padx=5, pady=5)
        H_button=tk.Button(self, text="H", command=self.fill_H)
        H_button.grid(column=1, row=0, padx=5, pady=5)

        # methods
        method_label=tk.Label(self,text="Method: ")
        method_label.grid(column=0, row=1, sticky=tk.E, padx=5, pady=5)
        methods = ("Heuristic", "MM algorithm", "ME algorithm","Proximal Gradient","Proximal")
        method_spin_box = ttk.Spinbox(self, width=12, textvariable=self.current_method, values=methods)
        method_spin_box.grid(column=1, row=1, sticky=tk.W, padx=5, pady=5)

        # beta
        beta_label=tk.Label(self,text="Beta: ")
        beta_label.grid(column=0, row=2, sticky=tk.E, padx=5, pady=5)
        beta_spin_box = ttk.Spinbox(self, width=12, textvariable=self.current_beta, from_=-2, to=2, wrap=False)
        beta_spin_box.grid(column=1, row=2, sticky=tk.W, padx=5, pady=5)

        # alpha
        alpha_label=tk.Label(self,text="Alpha: ")
        alpha_label.grid(column=0, row=3, sticky=tk.E, padx=5, pady=5)
        alpha_entry = tk.Entry(self, width=12, textvariable=self.current_alpha)
        alpha_entry.grid(column=1, row=3, sticky=tk.W, padx=5, pady=5)

        # results
        start_calculation_button=tk.Button(self, text="Launch", command=self.launch)
        start_calculation_button.grid(column=0, row=5, columnspan=2, padx=5, pady=5)

    def fill_V(self):
        # load the matrix
        V = self.get_target()
        # check if the matrix is non-negative
        if np.all(V>=0):
            # change button color
            V_button=tk.Button(self, text="V", bg="yellow", command=self.empty_V)
            V_button.grid(column=0, row=0, sticky=tk.E, padx=5, pady=5)
            # fill V
            self.V = V

    def empty_V(self):
        # change button color
        V_button=tk.Button(self, text="V", command=self.fill_V)
        V_button.grid(column=0, row=0, sticky=tk.E, padx=5, pady=5)
        # empty V
        self.V = None

    def fill_W(self):
        # load the matrix
        W = self.get_target()
        # check if the matrix is non-negative
        if np.all(W>=0):
            # change button color
            W_button=tk.Button(self, text="W", bg="yellow", command=self.empty_W)
            W_button.grid(column=1, row=0, sticky=tk.W, padx=5, pady=5)
            # fill W
            self.W = W

    def empty_W(self):
        # change button color
        W_button=tk.Button(self, text="W", command=self.fill_W)
        W_button.grid(column=1, row=0, sticky=tk.W, padx=5, pady=5)
        # empty W
        self.W = None

    def fill_H(self):
        # load the matrix
        H = self.get_target()
        # check if the matrix is non-negative
        if np.all(H>=0):
            # change button color
            H_button=tk.Button(self, text="H", bg="yellow", command=self.empty_H)
            H_button.grid(column=1, row=0, padx=5, pady=5)
            # fill H
            self.H = H

    def empty_H(self):
        # change button color
        H_button=tk.Button(self, text="H", command=self.fill_H)
        H_button.grid(column=1, row=0, padx=5, pady=5)
        # empty H
        self.H = None

    @staticmethod
    def get_target():
        """ Description:
                - upload matrice and convert it into numpy array
        """
        filepath = askopenfilename(title="Ouvrir un fichier",filetypes=[('Text files','*.txt'),('csv files','*.csv'),])
        target = np.loadtxt(fname=filepath, delimiter=",",dtype = 'float',skiprows=1) if filepath else None
        return target

    @staticmethod   
    def matrix_to_str(matrix:np.ndarray):
        """ Description:
                - convert a numpy array into a string  
            Args:
                - matrix matrix: numpy array to convert  
            Return:
                - text string: matrix as a string
        """
        text = "".join(["".join([str(row[x])+", " if x != len(row)-1 else str(row[x])+" \n" for x in range(len(row))]) for row in matrix])
        return text

    @staticmethod
    def save_file(filename,matrix):
        """ Description:
                - write a txt or csv file from a string
        """
        text = ToolBox.matrix_to_str(matrix)
        text_file = open("{}.txt".format(filename), "w")
        text_file.write(text)
        text_file.close()

    @staticmethod
    def pass_():
        pass

    @staticmethod
    def generator(size):
        mat = np.random.rand(size[0],size[1])
        return mat

    def launch(self):
        if self.V is not None and (self.W is None or self.H is None):
            ongoing_button=tk.Button(self, text="Making calculations", command=self.pass_)
            ongoing_button.grid(column=0, row=5, columnspan=2, padx=5, pady=5)
            if self.W is not None:
                self.H = self.generator((self.W.shape[1],self.V.shape[1]))
                ## TODO : implémenter méthode de factorisation avec W fixe
                pb = bd.nmf(self.V.copy()
                            ,self.W.copy()
                            ,self.H.copy()
                            ,"beta-div"
                            ,self.current_method.get()
                            ,self.current_beta.get()
                            ,float(self.current_alpha.get()))
                while pb.keep:
                    pb.maj()
                self.H = pb.H
            elif self.H is not None:
                self.W = self.generator((self.V.shape[0],self.H.shape[0]))
                ## TODO : implémenter méthode de factorisation avec H fixe
                pb = bd.nmf(self.V.copy()
                            ,self.W.copy()
                            ,self.H.copy()
                            ,"beta-div"
                            ,self.current_method.get()
                            ,self.current_beta.get()
                            ,float(self.current_alpha.get()))
                pb.switch()
                while pb.keep:
                    pb.maj()
                pb.switch()
                self.W = pb.W
            else:
                self.W = self.generator((self.V.shape[0],min(self.V.shape)))
                self.H = self.generator((min(self.V.shape),self.V.shape[1]))
                ## TODO : implémenter méthode de factorisation avec W et H inconnus
                pb = bd.nmf(self.V.copy()
                            ,self.W.copy()
                            ,self.H.copy()
                            ,"beta-div"
                            ,self.current_method.get()
                            ,self.current_beta.get()
                            ,float(self.current_alpha.get()))
                pb.epsW = np.ones(self.W.shape)
                pb.epsH = np.ones(self.H.shape)
                pb.lbd = 0
                pb.G = np.copy(pb.W)
                pb.F = pb.H.copy()
                while pb.keep:
                    pb.maj()
                    pb.switch()
                if self.V.any() != pb.V.any():
                    pb.switch()
                self.W = pb.W
                self.H = pb.H
            ongoing_button.destroy()
            import_button=tk.Button(self, text="Import results", command=self.import_res)
            import_button.grid(column=0, row=5, columnspan=2, padx=5, pady=5)

    def import_res(self): 
        self.save_file("W",self.W)
        self.save_file("H",self.H)
        self.destroy()
        self.__init__()
