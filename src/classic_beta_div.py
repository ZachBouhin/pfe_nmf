
from pprint import pprint
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 


class nmf(): 
    def __init__(self,V,W,H,metric,method,beta=0,nabla=0,eps=0.1):
        self.V = V
        self.W = W 
        self.H = H
        self.beta = beta
        self.nabla = nabla 
        self.eps = eps
        self.metric = metric
        self.method = str(method)
        self.keep = True
        self.Y = []
        self.epsW= 0 
        self.epsH = 0
        self.counter = 0

    def set_method(self,val): 
        self.method = val

    def switch(self):
        """ Allows W optimization by transposing matrices
        """
        self.V = self.V.transpose()
        inter = self.W.copy()
        inter2 = self.H.copy()
        self.W = inter2.transpose()
        self.H = inter.transpose()        
  

    def initialize_factorization(self):

        """ Initialize W and H in order to approximate V = W*H

        Args:
            V (Matrix): Matrix to approximate 
            size (Integer): Size we want to set for W(columns) and H(lines). 

        Returns:
            W (Matrix)
            H (Matrix)
        """


        N,M = np.size(self.V,axis=0),np.size(self.V,axis= 1)
        W,H = np.random.rand(N,self.size),np.random.rand(self.size,M)
        return W,H


    def d_beta(self,X,Y):

        """ Beta-Divergence function.

        Args:
            X (float): First value to compare
            Y (float): Second value to compare
            beta (float): Factor beta

        Returns:
            float : Evaluation of the beta-divergence fonction at (X,Y)
        """


        if self.beta == 1 : 
            if X!=0 and Y!=0 :
                return X*np.log(X/Y) -X+Y
            else : 
                return 0

        if self.beta == 0 : 
            if X!=0 and Y!=0 : 
                return X/Y - np.log(X/Y) - 1 
            else : 
                return 0

        else : 
            return (X**self.beta + (self.beta-1)*Y**self.beta - self.beta*X*Y**(self.beta-1))/(self.beta*(self.beta-1))



    def d_prime_beta(self,X,Y): 

        """ Derivative of the beta-divergence function as a function of y.

        Args:
            X (float)
            Y (float)
            beta (float): Beta factor for beta-divergence

        Returns:
            float: Evaluation of the beta-divergence function at (X,Y)
        """

        return Y**(self.beta-2) *(Y-X)



    def up_d(self,X,Y): 

        """ Convex part of beta-divergence function as a function of Y.

        Args:
            X (float)
            Y (float)
            beta (float): Beta factor

        Returns:
            float: Evaluation of the function at (X,Y)
        """

        if 0<self.beta<1 : 
            return (-1/(self.beta-1))*X*(Y**(self.beta-1))

        if self.beta == 0 : 
            return X/Y

        if 1<= self.beta <= 2 : 
            return self.d_beta()

        if self.beta > 2 : 
            return Y**self.beta/self.beta



    def up_prime_d(self,X,Y): 

        """ Derivative of up_d as a function of Y.

        Args:
            X (float)
            Y (float)
            beta (float): Beta factor

        Returns:
            float : Evaluation of the function at (X,Y)
        """

        if 0<self.beta<1 : 
            return -X*(Y**(self.beta-2))

        if self.beta == 0 : 
            return -X/Y**2

        if 1<= self.beta <= 2 : 
            return self.d_prime_beta(X,Y)

        if self.beta > 2 : 
            return Y**(self.beta-1)


    def down_d(self,X,Y): 

        """Concav part of the beta divergence function as a function of Y.

        Args:
            X (float)
            Y (float)
            beta (float): Beta factor

        Returns:
            float: Evaluation of the function at (X,Y)
        """

        if 0<self.beta<1 : 
            return Y**self.beta/self.beta

        if self.beta == 0 : 
            return np.log(Y)

        if 1<= self.beta <= 2 : 
            return 0

        if self.beta > 2 : 
            return -X*Y**(self.beta-1)/(self.beta-1)



    def down_prime_d(self,X,Y): 

        """ Derivative of down_d as a fuction of Y. 

        Args:
            X (float)
            Y (float)
            beta (float): Beta factor

        Returns:
            float : Evaluation of the function at (X,Y)
        """

        if 0<self.beta<1 : 
            return Y**(self.beta-1)
        if self.beta == 0 : 
            return 1/Y
        if 1<= self.beta <= 2 : 
            return 0
        if self.beta > 2 : 
            return -X*(Y**(self.beta-2))



    def bar_d(self,X): 

        """ Constant of the beta divergence function as a function of Y.

        Args:
            X (float)
            beta (float): Beta factor

        Returns:
            float : Evaluation of the function at X
        """

        if 0<self.beta<1 : 
            return X**(self.beta)/(self.beta*(self.beta-1))

        if self.beta == 0 : 
            return X*(np.log(X)-1)

        if 1<= self.beta <= 2 : 
            return 0

        if self.beta > 2 : 
            return X**(self.beta)/(self.beta*(self.beta-1))



    def criterion(self): 
        """ 
        Function calculating the distance between V and W*H 

        Args:
            V (matrix, size M*N): Matrix to factorize
            W (matrix, size M*k): First factor
            H (matrix, size k*N): Second factor
            metric (str) : Choosed metric, possible values : ['beta-div','mae','mse'], default = 'beta-div'
            beta (float) : Beta-factor for beta-divergence

        Returns: 
            crit (float) : Metric value between V and W*H
        """
        crit = 0 
        prod = np.dot(self.W,self.H)

        if self.metric == 'beta-div':
            for i in range(np.size(self.V,axis=0)):
                for j in range(np.size(self.V,axis=1)) :
                    crit+= self.d_beta(self.V[i,j],prod[i,j])

        if self.metric == 'mae': 
            for i in range(np.size(self.V,axis=0)):
                for j in range(np.size(self.V,axis=1)) :
                    crit+= np.abs(self.V[i,j]-prod[i,j])

        if self.metric == 'mse': 
            for i in range(np.size(self.V,axis=0)):
                for j in range(np.size(self.V,axis=1)) :
                    crit+= np.abs(self.V[i,j]-prod[i,j])**2

        return crit


    def gamma(self): 
        """ Beta correction

        Args:
            beta (Float)

        Returns:
            [Float]: Gamma(beta)
        """

        if self.beta < 1 :
            return(1/(2-self.beta))

        if 1 <= self.beta <= 2 : 
            return 1 

        if self.beta > 2 : 
            return(1/(self.beta-1))


    def MM_alg(self,h,v,new_v):

        """ Algorithm for the Maximization-minimization update.

        Args:
            h (Vector): Column of H
            W (Matrix)
            v (Vector): Column of V
            new_v (Vector): Column of W*H
            beta (float): Beta factor
            nabla (float): Regularization factor

        Returns : 
        new_h (vector): Updated column of h 
        """

        new_h =np.copy(h)
        for j in range(len(h)):
            up,down =0,0
            for i in range(np.size(self.W,axis=0)):
                up+=self.W[i,j]*v[i]*(new_v[i]**(self.beta-2))
                down += self.W[i,j]*(new_v[i]**(self.beta-1))
            if self.beta >=2 : 
                up += -self.nabla
            if self.beta <=1 : 
                down += self.nabla
            if down !=0 : 

                fact = h[j]*((up/down)**self.gamma())
            else: fact = 0 
            new_h[j]=fact
 
        return(new_h)


    def ME_alg(self,h,v,new_v): 

        """ Algorithm for the Maximization-minimization update.

        Args:
            h (Vector): Column of H
            W (Matrix)
            v (Vector): Column of V
            new_v (Vector): Column of W*H
            beta (float): Beta factor
            nabla (float): Regularization factor

        Returns : 
        new_h (vector): Updated column of h 
        """
        new_h =np.copy(h)
        if 1 >= self.beta >0 : 
            h_h = self.heuristic(h,v,new_v)
            for i in range(len(new_h)): 
                new_h[i] = (new_h[i]/4) * (np.sqrt(1+ 8*h_h[i]/new_h[i])-1)**2
        if 1 < self.beta <= 2 : 
            h_mm = self.MM_alg(h,v,new_v)
            for i in range(len(new_h)): 
                new_h[i] = (new_h[i]/4) *(np.sqrt(12*h_mm[i]/new_h[i] - 3)-1)**2
        return (new_h)
         


    def heuristic(self,h,v,new_v): 

        """ Algorithm for the Maximization-minimization update.

        Args:
            h (Vector): Column of H
            W (Matrix)
            v (Vector): Column of V
            new_v (Vector): Column of W*H
            beta (float): Beta factor
            nabla (float): Regularization factor

        Returns : 
        new_h (vector): Updated column of h 
        """
        new_h =np.copy(h)
        for j in range(len(h)):
            up,down =0,0
            for i in range(np.size(self.W,axis=0)):
                up+=self.W[i,j]*v[i]*(new_v[i]**(self.beta-2))
                down += self.W[i,j]*(new_v[i]**(self.beta-1))
            if self.beta >=2 : 
                up += -self.nabla
            if self.beta <1 : 
                down += self.nabla
            fact = h[j]*(up/down)
            new_h[j]=fact
 
        return(new_h)

    def gradient_desc(self):

        """ Optimization function with multiplacative update column by column.

        Args:
            V (matrix, size M*N): Matrix to factorize
            W (matrix, size M*k): First factor
            H (matrix, size k*N): Second factor
            beta (float) : Beta-factor for beta-divergence
            nabla (float) : Regularization factor for lasso, default : 0
            metric (str) : Choosed metric, possible values : ['beta-div','mae','mse'], default : 'beta-div'
            method (str) : Choosed update strategy, possible values : ['MM','ME','Heuristic'], default : 'MM'
        Returns:
            start (float) : Initial distance between V and W*H
            end (float) : Minimal distance reach during the loop
            Y (float) : List of all the values of the distance
        """

        start = self.criterion()
        counter = 0
        self.Y = [start]
        if self.method == 'MM algorithm': 
            while counter < 5000 : 

                new_V = np.dot(self.W,self.H)
                for i in range(np.size(new_V,axis=1)):
                    
                    h,v,new_v=self.H[:,i],self.V[:,i],new_V[:,i]
                    self.H[:,i] = self.MM_alg(h,v,new_v)

                end = self.criterion()
                self.Y.append(end)
                counter+=1
        
        if self.method == 'ME algorithm': 
            while counter < 5000 : 

                new_V = np.dot(self.W,self.H)
                for i in range(np.size(new_V,axis=1)):
                    
                    h,v,new_v=self.H[:,i],self.V[:,i],new_V[:,i]
                    self.H[:,i] = self.ME_alg(h,v,new_v)

                end = self.criterion()
                self.Y.append(end)
                counter+=1

        if self.method == 'Heuristic': 
            while counter < 5000 : 

                new_V = np.dot(self.W,self.H)
                for i in range(np.size(new_V,axis=1)):  
                    
                    h,v,new_v=self.H[:,i],self.V[:,i],new_V[:,i]
                    self.H[:,i] = self.heuristic(h,v,new_v)

                end = self.criterion()
                self.Y.append(end)
                counter+=1


        return start,end,self.Y


    def newH(self):

        """ Multplicative update of H

        Args:
            V (matrix, size M*N): Matrix to factorize
            W (matrix, size M*k): First factor
            H (matrix, size k*N): Second factor
            beta (float) : Beta-factor for beta-divergence

        Returns:
            H :  Updated H
        """
        WH = np.matmul(self.W,self.H)
        Wt = self.W.transpose()
        one = np.matmul(Wt,(WH**(self.beta-2))*self.V)
        second = np.matmul(Wt,WH**(self.beta-1))
        return self.H*((one/second)**self.gamma())


    def newW(self):
        """ Multplicative update of W

        Args:
            V (matrix, size M*N): Matrix to factorize
            W (matrix, size M*k): First factor
            H (matrix, size k*N): Second factor
            beta (float) : Beta-factor for beta-divergence

        Returns:
            W :  Updated W
        """

        WH = np.matmul(self.W,self.H)
        Ht = self.H.transpose()
        one = np.matmul((WH**(self.beta-2))*self.V,Ht)
        second = np.matmul(WH**(self.beta-1),Ht)
        return self.W*((one/second)**self.gamma())       


    def grad_desc2(self):
        """Optimization function with multiplacative update.

        Args:
            V (matrix, size M*N): Matrix to factorize
            W (matrix, size M*k): First factor
            H (matrix, size k*N): Second factor
            beta (float) : Beta-factor for beta-divergence

        Returns:
            start (float) : Initial distance between V and W*H
            end (float) : Minimal distance reach during the loop
        """

        start = self.criterion()
        counter = 0
        while counter < 30000: 

            self.H = self.newH()
            counter+= 1
            
        end = self.criterion()

        return start,end


    def aux_function(self,h,v,new_v): 
        """ Auxiliary function to verify hypothesis on updates.

        Args:
            W (Matrix)
            v (vector): column of V
            h (vector): column of H
            new_h (vector): column of W*H
            beta (float): Beta factor


        returns : 
            Evaluation of function 
        """

        first,sectot,ter = 0,0,0 
        new_v = np.dot(self.W,self.new_h)
        for i in range(len(v)): 
            sec=0
            for j in range(len(h)):
                first +=(self.W[i,j]*self.new_h[j]/new_v[i])* self.up_d(v[i],new_v[i]*h[j]/self.new_h[j])
                sec+= self.W[i,j]*(h[j]-self.new_h[j])
            sectot += self.down_prime_d(v[i],new_v[i]) *sec + self.down_d(v[i],new_v[i])
            ter+=self.bar_d(v[i])
        return(first + sectot + ter)




    def noisy_dkl(self,X,Y): 
        """ Noisy Kullback-leibler divergence function

        Args:
            X (float)
            Y (float)
            eps (float): Noise

        Returns:
            res (float): Evaluation of the function
        """

        res = 0 
        for i in range(len(X)): 
            res+=X[i]* np.log(X[i]/(Y[i]+self.eps)) - X[i] + Y[i] + self.eps

        return res



    def dualion_kl(self,h,v,new_v): 
        """ Dual for Kullbackleibler case

        Args:
            v (vector): Column of V
            teta (vector)
            nabla (float): Regularization factor
            eps (float): Noise

        Returns:
            float : Evaluation of the function at (v,teta)
        """
        if self.metric =="mse": 

            dual_res = np.linalg.norm(v,ord = 2)**2/2 - np.linalg.norm(v-self.nabla*self.teta,ord = 2)**2/2

        if self.metric =="beta-div": 
            if self.beta == 1 : 

                for i in range(len(v)): 
                    dual_res+= v[i]*np.log(1 + self.nabla*self.teta[i]) - self.eps*self.nabla*self.teta[i]
            if self.beta == 1.5 : 
                for i in range(len(v)): 
                    dual_res += - self.eps*self.nabla*self.teta[i]
                dual_res+= (np.linalg.norm(self.nabla * self.teta,ord =3 )**3)/6 - (np.linalg.norm(self.nabla*self.teta + 4*v,ord =1.5)**1.5)/6
                + self.nabla * np.dot(self.teta.transpose(),v) + 4/3* (np.linalg.norm(v, ord = 1.5)**1.5)
            if self.beta ==2 : 
                dual_res = np.linalg.norm(v,ord = 2)**2/2 - np.linalg.norm(v-self.nabla*self.teta,ord = 2)**2/2

            if self.beta == 0 : 
                dual_res = 0 
        return dual_res
        
    def get_teta(self,h,v,new_v): 
        """ Generation of the dual space values

        Args:
            h (vector): column of h
            W (matrix)
            nabla (float): Regularization factor    
            v (_type_): _description_

        Returns:
            _type_: _description_
        """
        if self.metric == "mse": 

            u = v - np.dot(self.W,h)
        
            return (self.nabla/np.linalg.norm(np.dot(np.transpose(self.W),u),ord = np.inf))*u 
        
        if self.metric =="beta-div": 
            if self.beta == 2 : 
                u = v - np.dot(self.W,h)
        
                return (self.nabla/np.linalg.norm(np.dot(np.transpose(self.W),u),ord = np.inf))*u 

            if self.beta == 1.5 : 
                u = - np.sqrt(np.dot(self.W,h) + np.array([self.eps for e in v]))
                l = []
                for i in range(len(v)): 

                    l.append(v[i]/(np.sqrt(np.dot(self.W[i,:],h))- self.eps)  )
                u = u + np.array(l)
                denom = max(np.dot(self.W.transpose(),v - np.array([self.eps for e in v])))/np.sqrt(self.eps)
                return(self.nabla / denom * u )
            if self.beta == 1 : 
                l=[]
                for i in range(len(v)):
                    l.append(v[i]/(np.dot(self.W[i,:],h))- self.eps) - 1 
                u = np.array(l) 
                denom = max(np.dot(self.W.transpose(),v - np.array([self.eps for e in v])))/(self.eps)
                return(self.nabla / denom * u )

    def teta_cond(self):  

        """ Condition for teta 

        Args:
            W (Matrix)
            teta (vector)
            nabla (float): Regularization factor

        Raises:
            ValueError: Error if teta doesn't fit the conditions
        """

        prod= np.dot(np.transpose(self.W),self.teta)
        for i in range(len(self.teta)): 
            if self.nabla*self.teta[i] >=-1 or prod[i]<= 1 : 
                raise ValueError
        return(True)

        
    def primalion(self,h,v,new_v): 
        """ Primal function for Kullback Leibler function

        Args:
            v (vector): column of V
            W (Matrix)
            h (vector): column of H
            beta (float): Beta factor
            nabla (float): Regularization factor
        """
        crit= 0
        prod = np.dot(self.W,h)
        if self.metric == 'beta_div': 


                for i in range(len(v)): 
                    crit += self.d_beta(v[i],prod[i])
                crit += np.linalg.norm(h,ord = 2)*self.nabla

        if self.metric == 'mse': 

            crit = np.linalg.norm(v-prod,ord = 2)**2/2
        
        return(crit)

    def dual_gap(self,h,v,new_v): 
        """ Function that determines the Dual gap.

        Args:
            v (vector): column of V
            W (Matrix)
            h (vector): colummn of H
            beta (float): Beta factor
            nabla (float): Regularization factor
            eps (float): Noise 
            Y (list): List to stock all iterations

        Returns:
            _type_: _description_
        """


        primal = self.primalion(h,v,new_v)
        
        self.teta = self.get_teta(h,v,new_v)
        dual = self.dualion_kl(h,v,new_v)
        self.Y.append(dual)
        return np.abs(primal- dual)



    def opt(self):

        """ Optimization function for the gap

        Args:
            V (Matrix)
            W (Matrix)
            H (Matrix)
            beta (float): Beta factor
            nabla (float): Regularization factor
            eps (float): Noise

        Returns:
            gap(float): Gap between dual and primal
        """    

        start = self.criterion()
        self.Y=[]
        self.gap = 10
        self.count = 0
        self.epsW = np.ones(self.W.shape)
        self.epsH = np.ones(self.H.shape)
        while self.keep: 
           self.count +=1 
           self.maj()
           self.switch()

        if self.count%2 ==1 : 
            self.switch()
        

        return self.gap

    def maj(self): 

        new_gap = 0

        if self.method =='MM algorithm': 
            self.new_V = np.dot(self.W,self.H)
                #if counter%2 == 0 :

            for i in range(np.size(self.new_V,axis=1)):

                    h,v,new_v=self.H[:,i].copy(),self.V[:,i].copy(),self.new_V[:,i].copy()
                    self.H[:,i] = self.MM_alg(h,v,new_v)
                    self.new_h = self.H[:,i].copy()
                    #new_gap += self.dual_gap(h,v,new_v)
                
            

        if self.method =='MM algorithms': 
            self.H = self.newH()
            self.new_V = np.dot(self.W,self.H)
            for i in range(np.size(self.new_V,axis=1)):
                h,v,new_v=self.H[:,i].copy(),self.V[:,i].copy(),self.new_V[:,i].copy()
                #new_gap += self.dual_gap(h,v,new_v)

            
        if self.method =='ME algorithm': 
            self.new_V = np.dot(self.W,self.H)

            for i in range(np.size(self.new_V,axis=1)):

                    h,v,new_v=self.H[:,i],self.V[:,i],self.new_V[:,i]
                    self.H[:,i] = self.ME_alg(h,v,new_v)
                    self.new_h = self.H[:,i]
                    #new_gap += self.dual_gap(h,v,new_v)

        if self.method =='Heuristic': 
            self.new_V = np.dot(self.W,self.H)

            for i in range(np.size(self.new_V,axis=1)):

                    h,v,new_v=self.H[:,i],self.V[:,i],self.new_V[:,i]
                    self.H[:,i] = self.heuristic(h,v,new_v)
                    self.new_h = self.H[:,i]
                    #new_gap += self.dual_gap(h,v,new_v)
            
        if self.method =="Proximal Gradient": 
            #self.H_t = self.H.transpose().copy()
            self.W_t = self.W.transpose().copy()
            #self.grad_W = np.dot((np.dot(self.W, self.H)-self.V),self.H_t)
            self.grad_H = np.dot(self.W_t,(np.dot(self.W, self.H)-self.V))
            #self.t_W = 1/ np.linalg.norm(np.dot(self.H,self.H_t),ord =2)
            self.t_H = 1/ np.linalg.norm(np.dot(self.W_t,self.W),ord =2)
            self.H = self.prox_grad().copy()
            #self.new_V = np.dot(self.W,self.H)

            """for i in range(np.size(self.V, axis=1)):
                h,v,new_v=self.H[:,i],self.V[:,i],self.new_V[:,i] 
                new_gap+= self.dual_gap(h,v,new_v)"""
            

        if self.method=="Proximal": 
            val=np.linalg.eig(np.dot(self.W.transpose(),self.W))[0]
            lip= np.max(val)
            ## -- 1 : Descent Step
            self.H = self.H -self.H*self.W.transpose()**(self.beta - 2)*(np.dot(self.W,self.H) - self.V)/lip
            ## -- 2 : Proximal Step
            self.H = self.H - (self.H/abs(self.H))*(self.nabla/lip)
            """f = lambda x:x if x>= self.nabla/lip else 0 
            self.H = f(self.H)"""
            self.new_V = np.dot(self.W,self.H)            
            """for i in range(np.size(self.V, axis=1)):
                h,v,new_v=self.H[:,i],self.V[:,i],self.new_V[:,i] 
                new_gap+= self.dual_gap(h,v,new_v)
            
"""
        if self.counter >=1000: 
            self.keep= False
        else : 
            self.counter+=1
        """if new_gap < 10e-6:
            self.keep = False
"""
    
    def prox_grad(self): 
        
        new_lbd = (1+ np.sqrt(1+ 4*self.lbd**2))/2 
        self.gma = (1- self.lbd)/new_lbd
        self.lbd = new_lbd


        """new_G = np.maximum(self.G - self.t_W * self.grad_W, self.epsW)
        self.W = (1-self.gma)*new_G.copy() + self.gma * self.G.copy()
        self.G = new_G.copy()"""

        new_F = np.maximum(self.H - self.t_H * self.grad_H, self.epsH)
        self.H = (1-self.gma)*new_F.copy() + self.gma * self.F.copy()
        self.F = new_F.copy()
        return self.H
        

        

"""         else : 
            for i in range(np.size(init_V.transpose(),axis=1)):
            
                w,v,new_v=W.transpose()[:,i],V.transpose()[:,i],init_V.transpose()[:,i]
                W.transpose()[:,i] = MM_alg(w,H.transpose(),v,new_v,nabla)
                new_w = W.transpose()[:,i]
                new_gap += dual_gap(v,H.transpose(),new_w,nabla,eps)"""



""" V = np.random.rand(25,20)

W,H = initialize_factorization(V,15)
self.beta = 2
nabla = 0.5
start,end,Y =gradient_desc(V,W,H,nabla)
print(start,end)

plt.plot(Y[2:100])
plt.xlabel('itérations')
plt.ylabel('distances')
plt.show()
"""

"""V = np.random.rand(20,20)

W,H = initialize_factorization(V,15)
beta = 2
nabla = 0.5
eps = 0
gap = opt(V,W,H,nabla,eps)
print(gap)"""


"""V = np.loadtxt("C:/Users/achan/pfe_nmf/Data/mat.csv",delimiter=",",dtype='float',skiprows=1)
W = np.random.rand(5,3)
H = np.random.rand(3,3)
bd = nmf(V,W,H,'beta-div','MM algorithm', beta = 2, nabla = 0.5)
print(bd.criterion())
bd.gradient_desc()

plt.plot(np.log(bd.Y[0:200]))
plt.title("Evolution de la distance en fonction du nombre d'itérations")
plt.xlabel("Nombre d'itérations")
plt.ylabel('Distance au sens Kullback-Leibler')
plt.show()
pprint(np.round(W,2))    
pprint(np.round(H,2))"""